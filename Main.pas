unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Menus, Grids, ComCtrls, ScktComp, ExtCtrls;

Const
  Cell_Height = 20;
  Cell_Width = 20;
  GamerPoleLeft = 20;
  GamerPoleTop = 20;
  ComputerPoleLeft = 320;
  ComputerPoleTop = 20;

type
  TBuild = (swAutoBuild, swNotAutoBuild);

  TShip = Class  //����� - �������
    Private
      AOrientaition :Boolean;
      ALive         :Boolean;
      ACountPalub   :Byte;
      AStatusPalub  :Array [1..4] Of Boolean;
      APlace        :TPoint;
    Public
      Constructor Create(orient: Boolean; countpal: Byte; pl: TPoint);  //�������� �������
      Procedure Show(owner_field: Boolean; Sender: TObject; OwnerForm: TForm);  //��������� �������
      Procedure Attack(Where: Byte);  //���� �� ������ �������
      Property Orientaition: Boolean Read AOrientaition Write AOrientaition; //���������� �������: true - �������������
      Property Live: Boolean Read ALive Write ALive;    //������� ������ - False
      Property CountPalub: Byte Read ACountPalub Write ACountPalub;  //����� �����
      Function GetStatusPalub(Index: Byte): Boolean;  //�������� ������ ������
      Procedure SetStatusPalub(Index: Byte; NewStatus: Boolean); //���������� ������ ������
      Property StatusPalub[Index:Byte]: Boolean Read GetStatusPalub Write SetStatusPalub; //������������ ������
      Property Place: TPoint Read APlace Write APlace; //���������� ������� ������������ �����
  End;

  TField = Class  //����� - ����
    Private
      AOwner  :Boolean;
      AMap    :Array [0..11, 0..11] Of Integer;
      APlace  :TPoint;
      AShips  :Array [1..10] Of TShip;
    Public
      Constructor Create(pl: TPoint; own: Boolean; Build: TBuild = swAutoBuild);  //�������� ����
      Destructor Destroy; Override; //����������� ����
      Procedure Show(Sender :TForm);  //��������� ����
      Procedure Fire(Boom: TPoint);  //������� �� ����
      Function CheckToDeath: Boolean;  //�������� �� ���������: True - ��������
      Property Owner: Boolean Read AOwner Write AOwner; //��� ����: true - ������
      Function GetMap(IndexX, IndexY: Byte): Integer;  //�������� ���������� ������ �����
      Procedure SetMap(IndexX, IndexY: Byte; NewMap: Integer); //���������� ���������� ������ �����
      Property Map[IndexX, IndexY:Byte]: Integer Read GetMap Write SetMap;  {����� ����: -2 - � ����� ��������
                                                                                         -1 - ����� �������
                                                                                          0 - ������ ������
                                                                                          1..10 - ���������� ����� �������}
      Property Place: TPoint Read APlace Write APlace; //���������� ���� ������������ �����
      Function GetShip(Index: Byte): TShip;  //�������� ��������� �������
      Procedure SetShip(Index: Byte; NewShip: TShip); //���������� ��������� �������
      Property Ships[Index: Byte]: TShip Read GetShip Write SetShip; //�������
  End;

  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Button1: TButton;
    StatusBar1: TStatusBar;
    Button2: TButton;
    ClientSocket1: TClientSocket;
    ServerSocket1: TServerSocket;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    N13: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormPaint(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ClientSocket1Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Disconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1ClientConnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1ClientDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Error(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure ServerSocket1ClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ClientSocket1Read(Sender: TObject; Socket: TCustomWinSocket);
    procedure N12Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;                                         

  TMessageNetWork = Record  //��������� �� ����
    Id: String[5]; //����������� �������
    WhoMove: Boolean;  //��� �����
    ShotX, ShotY: Byte;  //���������� ��������
    Init: LongInt;  //��������� ��������� ��������
    Crash: Boolean;  //��������� ��� ������� ����
  End;

var
  Form1: TForm1;
  GamerPole, ComputerPole: TField;  //���� ������ � ����������
  HodGamer: Boolean; //��� ���
  msg1, msg2, GamerName1, GamerName2, HostName: String;  //��������� �� ������, ����� ������� � ��� �����
  StatusGame: (swOnce, swServer, swClient, swHotSeat);  //��� ����
  CanPaint, GameGoes, GameEnd: Boolean;  //��������: ����� ��������, ���� ��������, ���� ��������
  BrainComputer: (swLow, swAverage, swHigh); //��������� ����������
  Shot, ShotPr: TPoint; //���������� �������� �������� � �����������
  FirstLuckyShot: TPoint;  //���������� ������� �������� �������� ��� ����������� ���������
  OrienCrash: (swHorizontal, swVertical, swUnKnow);  //���������� ������������ �������
  WayFire: Boolean;  //����������� ��������� �������: True - ����� ��� �����
  MessageNetWork: TMessageNetWork; //������� �� ����
  HasNewGame: Boolean; //������� ������ ����� ����, �� �������� ������������ ������ �������
  BuildShip: TBuild;  //����� ����������� ��������
//  PlaceMent1, PlaceMent2, PlaceMent3: Boolean;  //���������� ���������� �������
  WhatDeck: Byte;  //����� ������� ������
  HandShipOK: Boolean; //���������� ��� ��� ������� �������

implementation

{$R *.DFM}

Uses Running, Options, About, HandShip;

  //�������� ������ ������
  Function TShip.GetStatusPalub(Index: Byte): Boolean;
  Begin
    Result:=AStatusPalub[Index];
  End;

  //���������� ������ ������
  Procedure TShip.SetStatusPalub(Index: Byte; NewStatus: Boolean);
  Begin
    AStatusPalub[Index]:=NewStatus;
  End;

  //��������� �������
  Procedure TShip.Show(owner_field: Boolean; Sender: TObject; OwnerForm: TForm);
  Var
    i: Byte;
    ABrush: TBrush;
    X, Y: Integer;
  Begin
    //������ �������
    ABrush := OwnerForm.Canvas.Brush;  //���������� ��������� �����
    If Live Then Begin  //���� ������� ���
      If owner_field Then Begin  //���� ��� ����� ��������
        OwnerForm.Canvas.Brush.Color := clGreen;  //������ ���� �������
        For i:=0 To CountPalub-1 Do  //��� ���� �����
          If Orientaition Then  //���� ������� ��������� �������������
            OwnerForm.Canvas.Rectangle(Place.X+i*Cell_Width,Place.Y,Place.X+(i+1)*Cell_Width,Place.Y+Cell_Height)  //������ ������
          Else  //���� ������� ��������� �����������
            OwnerForm.Canvas.Rectangle(Place.X,Place.Y+i*Cell_Height,Place.X+Cell_Width,Place.Y+(i+1)*Cell_Height);  //������ ������
      End;  //��������� ����� �����
      //������ �������� ������
      OwnerForm.Canvas.Brush.Color := clRed;  //������ ���� �������
      For i:=0 To CountPalub-1 Do  //��� ���� �����
        If StatusPalub[i+1] Then  //���� ������ �������
          If Orientaition Then  //���� ������� ��������� �������������
            OwnerForm.Canvas.Rectangle(Place.X+i*Cell_Width,Place.Y,Place.X+(i+1)*Cell_Width,Place.Y+Cell_Height)  //������ ������
          Else  //���� ������� ��������� �����������
            OwnerForm.Canvas.Rectangle(Place.X,Place.Y+i*Cell_Height,Place.X+Cell_Width,Place.Y+(i+1)*Cell_Height); //������ ������
    End;  //��������� "�����" ��������
    //������ �������� �������
    OwnerForm.Canvas.Brush.Color := clBlack; //������ ���� �������
    If Not Live Then Begin  //������� ������
      If Orientaition Then  //���� ������� ��������� �������������
        OwnerForm.Canvas.Rectangle(Place.X,Place.Y,Place.X+CountPalub*Cell_Width,Place.Y+Cell_Height)   //������ �������
      Else  //���� ������� ��������� �����������
        OwnerForm.Canvas.Rectangle(Place.X,Place.Y,Place.X+Cell_Width,Place.Y+CountPalub*Cell_Height);  //������ �������
      //������� �������� ������� ��������
      OwnerForm.Canvas.Brush.Color := clBlack;  //������ ���� �������
      If (Sender = GamerPole) Then Begin  //���� ������ ������� ������
        X:=GamerPole.Place.x;  //����� ���������� ���� ������
        Y:=GamerPole.Place.y;
      End;
      If (Sender = ComputerPole) Then Begin  //���� ������ ������� ����������
        X:=ComputerPole.Place.x;  //����� ���������� ���� ����������
        Y:=ComputerPole.Place.y;
      End;
      If Orientaition Then Begin  //���� ������� ��������� �������������
        //��������� ������� �� �����  �������
        If Place.X > X + Cell_Width - 1 Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y+Cell_Height*0.25),
                               Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y+Cell_Height*0.75));
        If (Place.X > X + Cell_Width - 1) And (Place.Y > Y + Cell_Height - 1) Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y-Cell_Height*0.75),
                               Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y-Cell_Height*0.25));
        If (Place.X > X + Cell_Width - 1) And (Place.Y < Y + Cell_Height * 9) Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y+Cell_Height*1.25),
                               Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y+Cell_Height*1.75));
        If Place.X + CountPalub * Cell_Width < X + Cell_Width * 9 + 1  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.25),Trunc(Place.Y+Cell_Height*0.25),
                               Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.75),Trunc(Place.Y+Cell_Height*0.75));
        If (Place.X + CountPalub * Cell_Width < X + Cell_Width * 9 + 1) And (Place.Y > Y + Cell_Height - 1) Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.25),Trunc(Place.Y-Cell_Height*0.75),
                               Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.75),Trunc(Place.Y-Cell_Height*0.25));
        If (Place.X + CountPalub * Cell_Width < X + Cell_Width * 9 + 1) And (Place.Y < Y + Cell_Height * 9) Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.25),Trunc(Place.Y+Cell_Height*1.25),
                               Trunc(Place.X+Cell_Width*CountPalub+Cell_Width*0.75),Trunc(Place.Y+Cell_Height*1.75));
      End
      Else Begin   //���� ������� ��������� �����������
        //��������� ������� �� ����� �������
        If Place.Y > Y + Cell_Height - 1  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*0.25),Trunc(Place.Y-Cell_Height*0.75),
                               Trunc(Place.X+Cell_Width*0.75),Trunc(Place.Y-Cell_Height*0.25));
        If (Place.Y > Y + Cell_Height - 1) And (Place.X > X + Cell_Height - 1)  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y-Cell_Height*0.75),
                               Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y-Cell_Height*0.25));
        If (Place.Y > Y + Cell_Height - 1) And (Place.X < X + Cell_Height * 9)  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*1.25),Trunc(Place.Y-Cell_Height*0.75),
                               Trunc(Place.X+Cell_Width*1.75),Trunc(Place.Y-Cell_Height*0.25));
        If Place.Y + CountPalub * Cell_Height < Y + Cell_Height * 9 + 1  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*0.25),Trunc(Place.Y+Cell_Height*0.75+CountPalub*Cell_Height),
                               Trunc(Place.X+Cell_Width*0.75),Trunc(Place.Y+Cell_Height*0.25+CountPalub*Cell_Height));
        If (Place.Y + CountPalub * Cell_Height < Y + Cell_Height * 9 + 1) And (Place.X > X + Cell_Height - 1)  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y+Cell_Height*0.75+CountPalub*Cell_Height),
                               Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y+Cell_Height*0.25+CountPalub*Cell_Height));
        If (Place.Y + CountPalub * Cell_Height < Y + Cell_Height * 9 + 1) And (Place.X < X + Cell_Height * 9)  Then
          OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*1.25),Trunc(Place.Y+Cell_Height*0.75+CountPalub*Cell_Height),
                               Trunc(Place.X+Cell_Width*1.75),Trunc(Place.Y+Cell_Height*0.25+CountPalub*Cell_Height));
      End;
      For i:=0 To CountPalub-1 Do
        If Orientaition Then Begin
          If Place.Y > Y + Cell_Height - 1 Then
            OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*i+Cell_Width*0.25),Trunc(Place.Y-Cell_Height*0.75),
                                 Trunc(Place.X+Cell_Width*i+Cell_Width*0.75),Trunc(Place.Y-Cell_Height*0.25));
          If Place.Y < Y + Cell_Height * 9  Then
            OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*i+Cell_Width*0.25),Trunc(Place.Y+Cell_Height*1.25),
                                 Trunc(Place.X+Cell_Width*i+Cell_Width*0.75),Trunc(Place.Y+Cell_Height*1.75));
        End
        Else Begin
          If Place.X > X + Cell_Height - 1  Then
            OwnerForm.Canvas.Ellipse(Trunc(Place.X-Cell_Width*0.75),Trunc(Place.Y+Cell_Height*0.25+Cell_Height*i),
                                 Trunc(Place.X-Cell_Width*0.25),Trunc(Place.Y+Cell_Height*0.75+Cell_Height*i));
          If Place.X < X + Cell_Height * 9  Then
            OwnerForm.Canvas.Ellipse(Trunc(Place.X+Cell_Width*1.25),Trunc(Place.Y+Cell_Height*0.75+i*Cell_Height),
                                 Trunc(Place.X+Cell_Width*1.75),Trunc(Place.Y+Cell_Height*0.25+i*Cell_Height));
        End;
    End;
    OwnerForm.Canvas.Brush := ABrush;
  End;

  //���� �� �������
  Procedure TShip.Attack(Where: Byte);
  Var
    i: Byte;
  Begin
    StatusPalub[Where] := True;
    Live := False;
    For i:=1 To CountPalub do
      If Not StatusPalub[i] Then
        Live := True;
  End;

  //�������� �������
  Constructor TShip.Create(orient: Boolean; countpal: Byte; pl: TPoint);
  Var
    i: Byte;
  Begin
    Orientaition := orient;
    CountPalub := countpal;
    Live := True;
    For i:=1 To countpal Do
      StatusPalub[i]:=False;
    Place := pl;
  End;

  //�������� ����
  Constructor TField.Create(pl: TPoint; own: Boolean; Build: TBuild = swAutoBuild);
  Var
    i, j, k, x, y, num: Byte;
    ori, peresecheniy_net: Boolean;
    place_ship: TPoint;
  Begin
    Place := pl;
    Owner := own;
    For i:=0 To 11 Do
      For j:=0 To 11 Do
        Map[i,j] := 0;
    Case Build Of
    swAutoBuild    : Begin
                       //��������� ��������
                       k := 1;
                       Repeat
                         //���������� �����
                         Case k Of
                         1     : num := 4;
                         2,3   : num := 3;
                         4..6  : num := 2;
                         7..10 : num := 1;
                         End;
                         //���������� �������
                         ori := Random > 0.5;
                         //���������� �������
                         If ori Then Begin
                           x := Random(10-num+1) + 1;
                           y := Random(10) + 1;
                         End
                         Else Begin
                           x := Random(10) + 1;
                           y := Random(10-num+1) + 1
                         End;
                         //�������� �� ����������� � ������� ���������
                         peresecheniy_net := True;
                         If ori Then Begin
                           For i:=0 To num-1 Do
                             If Map[x+i,y] <> 0 Then peresecheniy_net := False;
                         End
                         Else Begin
                           For i:=0 To num-1 Do
                             If Map[x,y+i] <> 0 Then peresecheniy_net := False;
                         End;
                         //���� ��� ����������� � ������� ���������
                         If peresecheniy_net Then Begin
                           //��������� MAP ���������� ������� �������
                           If ori Then Begin
                             For i:=0 To num-1 Do
                               Map[x+i,y] := k;
                           End
                           Else Begin
                             For i:=0 To num-1 Do
                               Map[x,y+i] := k;
                           End;
                           //�������� ������� -1
                           If ori Then Begin
                             For i:=0 To num-1 Do Begin
                               If i = 0 Then Begin
                                 Map[x-1,y] := -1;
                                 Map[x-1,y-1] := -1;
                                 Map[x-1,y+1] := -1;
                               End;
                               Map[x+i,y-1] := -1;
                               Map[x+i,y+1] := -1;
                               If i = num-1 Then Begin
                                 Map[x+num,y] := -1;
                                 Map[x+num,y-1] := -1;
                                 Map[x+num,y+1] := -1;
                               End;
                             End;
                           End
                           Else Begin
                             For i:=0 To num-1 Do Begin
                               If i = 0 Then Begin
                                 Map[x,y-1] := -1;
                                 Map[x-1,y-1] := -1;
                                 Map[x+1,y-1] := -1;
                               End;
                               Map[x-1,y+i] := -1;
                               Map[x+1,y+i] := -1;
                               If i = num-1 Then Begin
                                 Map[x,y+num] := -1;
                                 Map[x-1,y+num] := -1;
                                 Map[x+1,y+num] := -1;
                               End;
                             End
                           End;
                           //���������� �������
                           place_ship.x := pl.x + Cell_Width * (x - 1);
                           place_ship.y := pl.y + Cell_Height * (y - 1);
                           //�������� �������
                           Ships[k] := TShip.Create(ori,num,place_ship);
                           k := k + 1;
                         End;
                       Until k > 10;
                     End;
    swNotAutoBuild : Begin
                       //����� �� ���� ������ ������ 
                     End;
    End;
  End;

  //����������� ����
  Destructor TField.Destroy;
  Var
    i: Byte;
  Begin
    For i:=1 To 10 Do
      Ships[i].Free;
    Inherited Destroy;
  End;

  //��������� ����
  Procedure TField.Show(Sender :TForm);
  Var
    ARect: TRect;
    ABrush: TBrush;
    i, j: Byte;
  Begin
    ABrush := Sender.Canvas.Brush;
    //������ ����
    Sender.Canvas.Brush.Color := clBlue;
    ARect := Rect(Place.X,Place.Y,Place.X+Cell_Height*10,Place.Y+Cell_Width*10);
    Sender.Canvas.Rectangle(ARect);
    For i:=0 To 10 Do Begin
      //������ �������������� �����
      Sender.Canvas.MoveTo(Place.X, Place.Y+i*Cell_Height);
      Sender.Canvas.LineTo(Place.X+Cell_Width*10,Place.Y+i*Cell_Height);
      //������ ������������ �����
      Sender.Canvas.MoveTo(Place.X+i*Cell_Width, Place.Y);
      Sender.Canvas.LineTo(Place.X+Cell_Width*i,Place.Y+10*Cell_Height);
    End;
    //������ �������
    Sender.Canvas.Brush.Color := clBlack;
    For i:=1 To 10 Do
      For j:=1 To 10 Do
        If Map[i,j] = -2 Then
          Sender.Canvas.Ellipse(Place.X+(i-1)*Cell_Width+5,Place.Y+(j-1)*Cell_Height+5,Place.X+(i-1)*Cell_Width+15,Place.Y+(j-1)*Cell_Height+15);
    Sender.Canvas.Brush := ABrush;
  End;

  //������� �� ����
  Procedure TField.Fire(Boom: TPoint);
  Var
    num: Byte; //����� ������
    x, y: Integer;
  Begin
  //�� �����
    If Map[boom.x,boom.y] <= 0 Then Begin
      Map[boom.x,boom.y] := -2;
      Form1.StatusBar1.Panels[1].Text := '�� �����';
      HodGamer := Not HodGamer;
    End
   //�����
    Else Begin
      Form1.StatusBar1.Panels[1].Text := '�����';
      //���������� ����� ������
      If Ships[Map[boom.x,boom.y]].Orientaition Then Begin
        x := Place.x + Cell_Width * (boom.x - 1);
        num := (x - Ships[Map[boom.x,boom.y]].Place.x) Div Cell_Width + 1;
      End
      Else Begin
        y := Place.y + Cell_Height * (boom.y - 1);
        num := (y - Ships[Map[boom.x,boom.y]].Place.y) Div Cell_Height + 1;
      End;
      Ships[Map[boom.x,boom.y]].Attack(num);
      HodGamer := HodGamer;
    End;
  End;

  //�������� �� ���������: True - ��������
  Function TField.CheckToDeath: Boolean;
  Var
    i: Byte;
    A: Boolean;
  Begin
    A := False;
    For i:=1 To 10 Do
      A := A Or Ships[i].Live;
    Result := Not A;
  End;

  //�������� ���������� ������ �����
  Function TField.GetMap(IndexX, IndexY: Byte): Integer;
  Begin
    Result := AMap[IndexX, IndexY];
  End;

  //���������� ���������� ������ �����
  Procedure TField.SetMap(IndexX, IndexY: Byte; NewMap: Integer);
  Begin
    AMap[IndexX, IndexY] := NewMap;
  End;

  //�������� ��������� �������
  Function TField.GetShip(Index: Byte): TShip;
  Begin
    Result := AShips[Index];
  End;

  //���������� ��������� �������
  Procedure TField.SetShip(Index: Byte; NewShip: TShip);
  Begin
    AShips[Index] := NewShip;
  End;

//��� ����������
Procedure ShotComputer;
Var
  i, j: Byte;
  k: Integer;
  WasCrash: Boolean; //��������� � ������� ��� �����, �� �� ����
  RV: Real;
  Label 1;
  //�������� �� ������� ���������
  Function DoubleCrash(boom: TPoint): Boolean;
  Var
    num, x, y: Byte;
  Begin
    If GamerPole.Map[Shot.x,Shot.y] = -2 Then Begin  //���� ��� ��������
      Result := True;
      Exit;
    End;
    If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin  //������ � �������� ������
      If GamerPole.Ships[GamerPole.Map[boom.x,boom.y]].Orientaition Then Begin
        x := GamerPole.Place.x + Cell_Width * (boom.x - 1);
        num := (x - GamerPole.Ships[GamerPole.Map[boom.x,boom.y]].Place.x) Div Cell_Width + 1;
      End
      Else Begin
        y := GamerPole.Place.y + Cell_Height * (boom.y - 1);
        num := (y - GamerPole.Ships[GamerPole.Map[boom.x,boom.y]].Place.y) Div Cell_Height + 1;
      End;
      If GamerPole.Ships[GamerPole.Map[boom.x,boom.y]].StatusPalub[num] Then Begin
        Result := True;
        Exit;
      End;
    End;
    if GamerPole.Map[Shot.x,Shot.y] = -1 Then Begin //����� ���� �������� �������
      //������� ��� ���� �� ������� ������� ������ ��������, ������� � ������
      If GamerPole.Map[Shot.x+1,Shot.y] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x+1,Shot.y]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x+1,Shot.y+1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x+1,Shot.y+1]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x,Shot.y+1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x,Shot.y+1]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x-1,Shot.y+1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x-1,Shot.y+1]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x-1,Shot.y] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x-1,Shot.y]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x-1,Shot.y-1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x-1,Shot.y-1]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x,Shot.y-1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x,Shot.y-1]].Live Then Begin
          Result := True;
          Exit;
        End;
      If GamerPole.Map[Shot.x+1,Shot.y-1] > 0 Then
        If Not GamerPole.Ships[GamerPole.Map[Shot.x+1,Shot.y-1]].Live Then Begin
          Result := True;
          Exit;
        End;
    End;
    Result := False;
  End;
  //���� ������ ������� �����, �� ����� ������ ���������� ������
  Function GetShotDefault: TPoint;
  Var
    i, j: Byte;
  Begin
    For i:=1 To 10 Do Begin
      For j:=1 To 10 Do Begin
        If Not DoubleCrash(Point(i,j)) Then
          Result := Point(i,j);
      End;
    End;
  End;
Begin
  //����� ��������� ����������
  Case BrainComputer Of
  swLow     : Begin
                //��������� ������� �������� ������ ��� �����
                Shot.x := Random(10) + 1;
                Shot.y := Random(10) + 1;
              End;
  swAverage : Begin
                k := 0;
                Repeat
                  //��������� ������� �������� ������ ��� �����
                  Shot.x := Random(10) + 1;
                  Shot.y := Random(10) + 1;
                  k := k + 1;
                  //���� � ��� ������ ��� ���� ��� ������� ����� �� ����� ������� ������, �� ������
                Until (Not DoubleCrash(Shot)) Or (k > 1000);
                If k > 999 Then
                  Shot := GetShotDefault;
              End;
  swHigh    : Begin
                //���� ���� �� ������� �������, �� ������
                WasCrash := False;
                For i:=1 To 10 Do Begin
                  For j:=1 To GamerPole.Ships[i].CountPalub Do Begin
                    If GamerPole.Ships[i].StatusPalub[j] And GamerPole.Ships[i].Live Then Begin
                      WasCrash := True;
                      Break;
                    End;
                    If WasCrash Then Break;
                  End;
                End;
                //���� � ������� ��� �� ������
                If Not WasCrash Then Begin
                  OrienCrash := swUnKnow;
                  k := 0;
                  Repeat
                    //��������� ������� �������� ������ ��� �����
                    Shot.x := Random(10) + 1;
                    Shot.y := Random(10) + 1;
                    k := k + 1;
                    //���� � ��� ������ ��� ���� ��� ������� ����� �� ����� ������� ������, �� ������
                  Until (Not DoubleCrash(Shot)) Or (k > 1000);
                  If k > 999 Then
                    Shot := GetShotDefault;
                  If GamerPole.Map[Shot.x,Shot.y] > 0 Then
                    FirstLuckyShot := Shot;
                End
                //���� ���� �� ������� �������
                Else Begin
                  Case OrienCrash Of
                  //���� ���������� �� ��������
                  swUnKnow     : Begin
                                   k := 0;
                                   1:
                                   RV := Random;
                                   If (RV >= 0) And (RV < 0.25) Then Begin
                                     Shot.y := FirstLuckyShot.y;
                                     If FirstLuckyShot.x > 1 Then Begin
                                       Shot.x := FirstLuckyShot.x - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := True;
                                         OrienCrash := swHorizontal;
                                       End;
                                       If DoubleCrash(Shot) Then Begin
                                         k := k + 1;
                                         If k < 100 Then
                                           GoTo 1;
                                       End;
                                     End
                                     Else Begin
                                       Shot.x := FirstLuckyShot.x + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := False;
                                         OrienCrash := swHorizontal;
                                       End;
                                     End;
                                   End;
                                   If (RV >= 0.25) And (RV < 0.5) Then Begin
                                     Shot.y := FirstLuckyShot.y;
                                     If FirstLuckyShot.x < 10 Then Begin
                                       Shot.x := FirstLuckyShot.x + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := False;
                                         OrienCrash := swHorizontal;
                                       End;
                                       If DoubleCrash(Shot) Then Begin
                                         k := k + 1;
                                         If k < 100 Then
                                           GoTo 1;
                                       End;
                                     End
                                     Else Begin
                                       Shot.x := FirstLuckyShot.x - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := True;
                                         OrienCrash := swHorizontal;
                                       End;
                                     End;
                                   End;
                                   If (RV >= 0.5) And (RV < 0.75) Then Begin
                                     Shot.x := FirstLuckyShot.x;
                                     If FirstLuckyShot.y > 1 Then Begin
                                       Shot.y := FirstLuckyShot.y - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := True;
                                         OrienCrash := swVertical;
                                       End;
                                       If DoubleCrash(Shot) Then Begin
                                         k := k + 1;
                                         If k < 100 Then
                                           GoTo 1;
                                       End;
                                     End
                                     Else Begin
                                       Shot.y := FirstLuckyShot.y + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := False;
                                         OrienCrash := swVertical;
                                       End;
                                     End;
                                   End;
                                   If (RV >= 0.75) And (RV < 1) Then Begin
                                     Shot.x := FirstLuckyShot.x;
                                     If FirstLuckyShot.y < 10 Then Begin
                                       Shot.y := FirstLuckyShot.y + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := False;
                                         OrienCrash := swVertical;
                                       End;
                                       If DoubleCrash(Shot) Then Begin
                                         k := k + 1;
                                         If k < 100 Then
                                           GoTo 1;
                                       End;
                                     End
                                     Else Begin
                                       Shot.y := FirstLuckyShot.y - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] > 0 Then Begin
                                         WayFire := True;
                                         OrienCrash := swVertical;
                                       End;
                                     End;
                                   End;
                                   ShotPr := Shot;
                                 End;
                  swHorizontal : Begin
                                   Shot.y := ShotPr.y;
                                   //���� �������� �����
                                   If WayFire Then Begin
                                     If ShotPr.x - 1 >= 1 Then Begin
                                       Shot.x := ShotPr.x - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                                         WayFire := Not WayFire;
                                         ShotPr := FirstLuckyShot;
                                       End
                                       Else Begin
                                         ShotPr := Shot;
                                       End;
                                     End
                                     Else Begin
                                       Shot.x := FirstLuckyShot.x + 1;
                                       WayFire := Not WayFire;
                                       ShotPr := Shot;
                                     End;
                                   End
                                   Else Begin
                                   //���� �������� ������
                                     If ShotPr.x + 1 <= 10 Then Begin
                                       Shot.x := ShotPr.x + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                                         WayFire := Not WayFire;
                                         ShotPr := FirstLuckyShot;
                                       End
                                       Else Begin
                                         ShotPr := Shot;
                                       End;
                                     End
                                     Else Begin
                                       Shot.x := FirstLuckyShot.x - 1;
                                       WayFire := Not WayFire;
                                       ShotPr := Shot;
                                     End;
                                   End;
                                 End;
                  swVertical   : Begin
                                   Shot.x := ShotPr.x;
                                   //���� �������� �����
                                   If WayFire Then Begin
                                     If ShotPr.y - 1 >= 1 Then Begin
                                       Shot.y := ShotPr.y - 1;
                                       If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                                         WayFire := Not WayFire;
                                         ShotPr := FirstLuckyShot;
                                       End
                                       Else Begin
                                         ShotPr := Shot;
                                       End;
                                     End
                                     Else Begin
                                       Shot.y := FirstLuckyShot.y + 1;
                                       WayFire := Not WayFire;
                                       ShotPr := Shot;
                                     End;
                                   End
                                   Else Begin
                                   //���� �������� ����
                                     If ShotPr.y + 1 <= 10 Then Begin
                                       Shot.y := ShotPr.y + 1;
                                       If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                                         WayFire := Not WayFire;
                                         ShotPr := FirstLuckyShot;
                                       End
                                       Else Begin
                                         ShotPr := Shot;
                                       End;
                                     End
                                     Else Begin
                                       Shot.y := FirstLuckyShot.y - 1;
                                       WayFire := Not WayFire;
                                       ShotPr := Shot;
                                     End;
                                   End;
                                 End;
                  End;
                End;
              End;
  End;
End;

//������� ����
Procedure Game;
Var
  i, CountLiveGame, CountLiveComputer: Byte;
Begin
//� ����� ���� ������
  Case StatusGame Of
  swOnce    : Begin
                //��� ���
                If HodGamer Then Begin
                  //��� ������
                  Form1.StatusBar1.Panels[0].Text := msg1;
                  ComputerPole.Fire(Shot);
                  ComputerPole.Show(Form1);
                  For i:=1 To 10 Do
                    ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                  GameEnd := ComputerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ��������!');
                  End;
                End
                Else Begin
                  //��� ����������
                  Form1.StatusBar1.Panels[0].Text := msg2;
                  ShotComputer;
                  GamerPole.Fire(Shot);
                  GamerPole.Show(Form1);
                  For i:=1 To 10 Do
                    GamerPole.Ships[i].Show(true,GamerPole,Form1);
                  GameEnd := GamerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ���������!');
                  End;
                End;
                If HodGamer Then
                  Form1.StatusBar1.Panels[0].Text := msg1
                Else
                  Form1.StatusBar1.Panels[0].Text := msg2;
                If (Not HodGamer) And ( Not GameEnd) Then
                  Game;
              End;
  swServer  : Begin
                If HodGamer Then Begin
                  Form1.StatusBar1.Panels[0].Text := msg1;
                  ComputerPole.Fire(Shot);
                  ComputerPole.Show(Form1);
                  For i:=1 To 10 Do
                    ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                  GameEnd := ComputerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ��������!');
                  End;
                  //���������� ������ � �������� �� ����
                  MessageNetWork.Id := 'Shot';
                  MessageNetWork.ShotX := Shot.x;
                  MessageNetWork.ShotY := Shot.y;
                  If ComputerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                    MessageNetWork.Crash := False;
                    Form1.StatusBar1.Panels[0].Text := msg2;
                  End
                  Else Begin
                    MessageNetWork.Crash := True;
                    Form1.StatusBar1.Panels[0].Text := msg1;
                  End;
                  Form1.ServerSocket1.Socket.Connections[0].SendBuf(MessageNetWork,SizeOf(MessageNetWork));
                End
                Else Begin
                  Form1.StatusBar1.Panels[0].Text := msg2;
                  GamerPole.Fire(Shot);
                  ComputerPole.Show(Form1);
                  GamerPole.Show(Form1);
                  For i:=1 To 10 Do Begin
                    ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                    GamerPole.Ships[i].Show(true,GamerPole,Form1);
                  End;
                  GameEnd := GamerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ���������!');
                  End;
                End;
              End;
  swClient  : Begin
                If HodGamer Then Begin
                  Form1.StatusBar1.Panels[0].Text := msg1;
                  GamerPole.Fire(Shot);
                  GamerPole.Show(Form1);
                  For i:=1 To 10 Do
                    GamerPole.Ships[i].Show(false,GamerPole,Form1);
                  GameEnd := GamerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ��������!');
                  End;
                  //���������� ������ � �������� �� ����
                  MessageNetWork.Id := 'Shot';
                  MessageNetWork.ShotX := Shot.x;
                  MessageNetWork.ShotY := Shot.y;
                  If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                    MessageNetWork.Crash := False;
                    Form1.StatusBar1.Panels[0].Text := msg2;
                  End
                  Else Begin
                    MessageNetWork.Crash := True;
                    Form1.StatusBar1.Panels[0].Text := msg1;
                  End;
                  Form1.ClientSocket1.Socket.SendBuf(MessageNetWork,SizeOf(MessageNetWork));
                End
                Else Begin
                  Form1.StatusBar1.Panels[0].Text := msg2;
                  ComputerPole.Fire(Shot);
                  GamerPole.Show(Form1);
                  ComputerPole.Show(Form1);
                  For i:=1 To 10 Do Begin
                    GamerPole.Ships[i].Show(false,GamerPole,Form1);
                    ComputerPole.Ships[i].Show(true,ComputerPole,Form1);
                  End;
                  GameEnd := ComputerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('�� ���������!');
                  End;
                End;
              End;
  swHotSeat : Begin
                //��� ���
                If HodGamer Then Begin
                  //��� ������1
                  Form1.StatusBar1.Panels[0].Text := msg1;
                  ComputerPole.Fire(Shot);
                  ComputerPole.Show(Form1);
                  GamerPole.Show(Form1);
                  For i:=1 To 10 Do Begin
                    ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                    GamerPole.Ships[i].Show(true,GamerPole,Form1);
                  End;
                  GameEnd := ComputerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('������� '+GamerName1+' !');
                    Exit;
                  End;
                  If ComputerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                  //���� ������� ����, �� �����������
                    ComputerPole.Show(Form1);
                    GamerPole.Show(Form1);
                    For i:=1 To 10 Do Begin
                      ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                      GamerPole.Ships[i].Show(false,GamerPole,Form1);
                    End;
                    If MessageDlg('��� ������ '+GamerName2, mtInformation, [mbYes], 0) = mrYes Then Begin
                      ComputerPole.Show(Form1);
                      GamerPole.Show(Form1);
                      For i:=1 To 10 Do Begin
                        ComputerPole.Ships[i].Show(true,ComputerPole,Form1);
                        GamerPole.Ships[i].Show(false,GamerPole,Form1);
                      End;
                    End;
                  End;
                End
                Else Begin
                  //��� ������2
                  Form1.StatusBar1.Panels[0].Text := msg2;
                  GamerPole.Fire(Shot);
                  GamerPole.Show(Form1);
                  ComputerPole.Show(Form1);
                  For i:=1 To 10 Do Begin
                    GamerPole.Ships[i].Show(false,GamerPole,Form1);
                    ComputerPole.Ships[i].Show(true,ComputerPole,Form1);
                  End;
                  GameEnd := GamerPole.CheckToDeath;
                  If GameEnd Then Begin
                    GameGoes := False;
                    ShowMessage('������� '+GamerName2+' !');
                    Exit;
                  End;
                  If GamerPole.Map[Shot.x,Shot.y] <= 0 Then Begin
                  //���� ������� ����, �� �����������
                    ComputerPole.Show(Form1);
                    GamerPole.Show(Form1);
                    For i:=1 To 10 Do Begin
                      ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                      GamerPole.Ships[i].Show(false,GamerPole,Form1);
                    End;
                    If MessageDlg('��� ������ '+GamerName1, mtInformation, [mbYes], 0) = mrYes Then Begin
                      ComputerPole.Show(Form1);
                      GamerPole.Show(Form1);
                      For i:=1 To 10 Do Begin
                        ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                        GamerPole.Ships[i].Show(true,GamerPole,Form1);
                      End;
                    End;

                  End;
                End;
              End;
  End;
  //������� �������� �������� � �����
  CountLiveGame := 0;
  CountLiveComputer := 0;
  For i:=1 To 10 Do Begin
    If GamerPole.Ships[i].Live Then
      Inc(CountLiveGame);
    If ComputerPole.Ships[i].Live Then
      Inc(CountLiveComputer);
  End;
  If StatusGame <> swHotSeat Then Begin
    Form1.StatusBar1.Panels[2].Text := '� ��� ' + IntToStr(CountLiveGame) + ' ��������';
    Form1.StatusBar1.Panels[3].Text := '� ���������� ' + IntToStr(CountLiveComputer) + ' ��������';
  End
  Else Begin
    Form1.StatusBar1.Panels[2].Text := '� ������ ' + GamerName1 + ' ' + IntToStr(CountLiveGame) + ' ��������';
    Form1.StatusBar1.Panels[3].Text := '� ������ ' + GamerName2 + ' ' + IntToStr(CountLiveComputer) + ' ��������';
  End;
End;

//������ ����������� ��������
Procedure Hand_PlaceMent;
Begin
  GamerPole.Show(Form1);
  If (Form4.ShowModal = mrOk) And (HandShipOK) Then Begin
  End
  Else Begin
    GamerPole := TField.Create(Point(GamerPoleLeft,GamerPoleTop), True);
  End;
End;

//����� ����
Procedure NewGame;
Var
  i: Byte;
  RV: LongInt;
begin
  Randomize;
  //������������� ��������� �������� ��� ��������� �����
  RV := Random(2147483647);
  RandSeed := RV;
  //�������� ���� ������
  GamerPole.Free;
  GamerPole := TField.Create(Point(GamerPoleLeft,GamerPoleTop), True, BuildShip);
  //�������� ���� ����������
  ComputerPole.Free;
  ComputerPole := TField.Create(Point(ComputerPoleLeft,ComputerPoleTop), False);
  Case BuildShip Of
  swAutoBuild    : Begin
                     //����� ������ ������ �� ����
                   End;
  swNotAutoBuild : Begin
                     Hand_PlaceMent;
                   End;
  End;
  //����� ��������
  If Random > 0.5 Then Begin
    HodGamer := True;
    Form1.StatusBar1.Panels[0].Text := msg1;
  End
  Else Begin
    HodGamer := False;
    Form1.StatusBar1.Panels[0].Text := msg2;
  End;
  //����������� ����� � ������ ����
  Case StatusGame Of
  swOnce    : Begin
                Form1.Label1.Caption := '���� ������';
                Form1.Label2.Caption := '���� ����������';
                CanPaint := True;
                GameGoes := True;
                GamerPole.Show(Form1);
                For i:=1 To 10 Do
                  GamerPole.Ships[i].Show(GamerPole.Owner,GamerPole,Form1);
                ComputerPole.Show(Form1);
                For i:=1 To 10 Do
                  ComputerPole.Ships[i].Show(ComputerPole.Owner,ComputerPole,Form1);
                If Not HodGamer Then
                  Game;
              End;
  swServer  : Begin
                Form1.Label1.Caption := '���� ����';
                Form1.Label2.Caption := '���� ���������';
                CanPaint := True;
                GameGoes := True;
                GamerPole.Show(Form1);
                ComputerPole.Show(Form1);
                For i:=1 To 10 Do Begin
                  GamerPole.Ships[i].Show(true,GamerPole,Form1);
                  ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                End;
                //���������� ������ � �������� �� ����
                MessageNetWork.Id := 'New';
                MessageNetWork.WhoMove := HodGamer;
                MessageNetWork.Init := RV;
                If Form1.ServerSocket1.Socket.ActiveConnections <> 0 Then
                  Form1.ServerSocket1.Socket.Connections[0].SendBuf(MessageNetWork,SizeOf(MessageNetWork))
                Else
                  ShowMessage('�� ���� ������ ����. ��� ���������.'+Chr(13)+'����� ����������� ������ ������� ���� ������.');
              End;
  swClient  : Begin
                Form1.Label1.Caption := '���� ���������';
                Form1.Label2.Caption := '���� ����';
              End;
  swHotSeat : Begin
                Form1.Label1.Caption := '���� ������ ' + GamerName1;
                Form1.Label2.Caption := '���� ������ ' + GamerName2;
                If HodGamer Then
                  ShowMessage('��� ������ '+GamerName1)
                Else
                  ShowMessage('��� ������ '+GamerName2);
                GamerPole.Show(Form1);
                ComputerPole.Show(Form1);
                If HodGamer Then Begin
                  For i:=1 To 10 Do  Begin
                    GamerPole.Ships[i].Show(True,GamerPole,Form1);
                    ComputerPole.Ships[i].Show(False,ComputerPole,Form1);
                  End;
                End
                Else Begin
                  For i:=1 To 10 Do Begin
                    GamerPole.Ships[i].Show(False,GamerPole,Form1);
                    ComputerPole.Ships[i].Show(True,ComputerPole,Form1);
                  End;
                End;
              End;
  End;
  HasNewGame := False;
  CanPaint := True;
  GameGoes := True;
  If StatusGame <> swHotSeat Then Begin
    Form1.StatusBar1.Panels[2].Text := '� ��� 10 ��������';
    Form1.StatusBar1.Panels[3].Text := '� ���������� 10 ��������';
  End
  Else Begin
    Form1.StatusBar1.Panels[2].Text := '� ������ ' + GamerName1 + ' 10 ��������';
    Form1.StatusBar1.Panels[3].Text := '� ������ ' + GamerName2 + ' 10 ��������';
  End;
End;

//������ "����� ����"
procedure TForm1.Button1Click(Sender: TObject);
begin
  NewGame;
end;

//�������� ��� �������� �����
procedure TForm1.FormCreate(Sender: TObject);
begin
  CanPaint := False;
  GameGoes := False;
  GameEnd := False;
  BrainComputer := swHigh;
  Case StatusGame Of
  swOnce    : Begin
                msg1 := '��� ���';
                msg2 := '��� ����������';
                Button2.Enabled := True;
              End;
  swServer  : Begin
                msg1 := '��� ���';
                msg2 := '��� ���������';
                Button2.Enabled := False;
                N9.Enabled := False;
                ServerSocket1.Active := True;
              End;
  swClient  : Begin
                msg1 := '��� ���';
                msg2 := '��� ���������';
                Button2.Enabled := False;
                N9.Enabled := False;
                Button1.Enabled := False;
                ClientSocket1.Host := HostName;
                If ClientSocket1.Active Then
                  ClientSocket1.Active := False;
                ClientSocket1.Active := True;
              End;
  swHotSeat : Begin
                msg1 := '��� ' + GamerName1;
                msg2 := '��� ' + GamerName2;
                Button2.Enabled := False;
                N9.Enabled := False;
              End;
  End;
  MessageNetWork.Id := '';
end;

//�������� ��� ������
procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  If MessageDlg('�� ������������� ������ �������� ����?', mtInformation, [mbYes,mbNo], 0) = mrYes Then Begin
    CanClose := True;
    Form2.Close;
  End
  Else
    CanClose := False;
end;

//�������� ��� ����������� �����
procedure TForm1.FormPaint(Sender: TObject);
Var
  i: Byte;
begin
  If CanPaint Then Begin
    GamerPole.Show(Form1);
    ComputerPole.Show(Form1);
    Case StatusGame Of
    swOnce, swServer : Begin
                         For i:=1 To 10 Do Begin
                           GamerPole.Ships[i].Show(true,GamerPole,Form1);
                           ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                         End;
                       End;
    swClient         : Begin
                         For i:=1 To 10 Do Begin
                           GamerPole.Ships[i].Show(false,GamerPole,Form1);
                           ComputerPole.Ships[i].Show(True,ComputerPole,Form1);
                         End;
                       End;
    swHotSeat        : Begin
                         If (Not HasNewGame) Or (GameGoes) Then Begin
                           If HodGamer Then Begin
                             For i:=1 To 10 Do Begin
                               GamerPole.Ships[i].Show(true,GamerPole,Form1);
                               ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
                             End;
                           End
                           Else Begin
                             For i:=1 To 10 Do Begin
                               GamerPole.Ships[i].Show(false,GamerPole,Form1);
                               ComputerPole.Ships[i].Show(True,ComputerPole,Form1);
                             End;
                           End;
                         End;
                       End;
    End;
  End;
end;

//��������� ����
procedure TForm1.Button2Click(Sender: TObject);
begin
  If Form3.ShowModal = mrOk Then Begin
    If Form3.RadioButton1.Checked Then
      BrainComputer := swLow;
    If Form3.RadioButton2.Checked Then
      BrainComputer := swAverage;
    If Form3.RadioButton3.Checked Then
      BrainComputer := swHigh;
  End;
end;

//��� ������
procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  //������ �������� ������: ���� ������� ��� ����, �� x=y=-1
  Function GetShot(WhatPole: TField): TPoint;
  Begin
    //������ ������ ��� �����
    If ((X >= WhatPole.Place.x) And (X <= WhatPole.Place.x + Cell_Width * 10)) And
       ((Y >= WhatPole.Place.y) And (Y <= WhatPole.Place.y + Cell_Height * 10)) Then Begin
      //����� �������� ������������ ����
      GetShot.x := (X - WhatPole.Place.x) Div Cell_Width + 1;
      GetShot.y := (Y - WhatPole.Place.y) Div Cell_Height + 1;
    End
    Else
      GetShot := Point(-1,-1);
  End;
begin
  Case StatusGame Of
  swOnce    : Begin
              //��������� ��� ������
                If GameGoes And HodGamer Then Begin
                  Shot := GetShot(ComputerPole);
                  If (Shot.x <> -1) And (Shot.x <> -1) Then
                    Game;
                End;
              End;
  swServer  : Begin
              //��������� ��� ������
                If GameGoes And HodGamer Then Begin
                  Shot := GetShot(ComputerPole);
                  If (Shot.x <> -1) And (Shot.x <> -1) Then
                    Game;
                End;
              End;
  swClient  : Begin
              //��������� ��� ������
                If GameGoes And HodGamer Then Begin
                  Shot := GetShot(GamerPole);
                  If (Shot.x <> -1) And (Shot.x <> -1) Then
                    Game;
                End;
              End;
  swHotSeat : Begin
              //��������� ��� ������1
                If GameGoes And HodGamer Then Begin
                  Shot := GetShot(ComputerPole);
                  If (Shot.x <> -1) And (Shot.x <> -1) Then
                    Game;
                End;
              //��������� ��� ������2
                If GameGoes And (Not HodGamer) Then Begin
                  Shot := GetShot(GamerPole);
                  If (Shot.x <> -1) And (Shot.x <> -1) Then
                    Game;
                End;
              End;
  End;
end;

//�������� ��� ���������� � ��������
procedure TForm1.ClientSocket1Connect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  ShowMessage('����� � �������� �����������!');
end;

//�������� ��� ������������� � ��������
procedure TForm1.ClientSocket1Disconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  ShowMessage('����� � �������� ���������!');
end;

//�������� ��� ����������� �������
procedure TForm1.ServerSocket1ClientConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  ShowMessage('����������� ��������!');
end;

//�������� ��� ���������� �������
procedure TForm1.ServerSocket1ClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  ShowMessage('�������� ����������!');
end;

//�������� ��� ������ ������ �������� �������
procedure TForm1.ClientSocket1Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  Case ErrorEvent Of
  eeGeneral    : ShowMessage('���-�� ��������� �� ������');
  eeSend       : ShowMessage('�� ���� ��������� ��������� �������');
  eeReceive    : ShowMessage('�� ���� �������� ��������� �� �������');
  eeConnect    : ShowMessage('�� ���� ����� ������');
  eeDisconnect : ShowMessage('�� ���� ������������ �� �������');
  eeAccept     : ShowMessage('����� ������ �� ����');
  End;
end;

//�������� ��� ������ ������ �������
procedure TForm1.ServerSocket1ClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  Case ErrorEvent Of
  eeGeneral    : ShowMessage('���-�� ��������� �� ������');
  eeSend       : ShowMessage('�� ���� ��������� ��������� �� ����');
  eeReceive    : ShowMessage('�� ���� �������� ��������� �� ����');
  eeConnect    : ShowMessage('�� ���� ����������');
  eeDisconnect : ShowMessage('�� ���� ������������');
  eeAccept     : ShowMessage('��� ������� �� ����');
  End;
end;

//��������� ��������� �� �������
procedure TForm1.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
Var
  i: Byte;  
begin
  Form1.ServerSocket1.Socket.Connections[0].ReceiveBuf(MessageNetWork,SizeOf(MessageNetWork));
  If MessageNetWork.Id = 'Shot' Then Begin
    If MessageNetWork.Crash Then Begin
      Shot.x := MessageNetWork.ShotX;
      Shot.y := MessageNetWork.ShotY;
      HodGamer := False;
      Form1.StatusBar1.Panels[0].Text := msg2;
      Game;
    End
    Else Begin
      Form1.StatusBar1.Panels[0].Text := msg1;
      GamerPole.Fire(Point(MessageNetWork.ShotX,MessageNetWork.ShotY));
      GamerPole.Show(Form1);
      ComputerPole.Show(Form1);
      For i:=1 To 10 Do Begin
        GamerPole.Ships[i].Show(true,GamerPole,Form1);
        ComputerPole.Ships[i].Show(false,ComputerPole,Form1);
      End;
      HodGamer := True;
    End;
  End;
end;

//��������� ��������� �� �������
procedure TForm1.ClientSocket1Read(Sender: TObject;
  Socket: TCustomWinSocket);
Var
  i: Byte;  
begin
  Form1.ClientSocket1.Socket.ReceiveBuf(MessageNetWork,SizeOf(MessageNetWork));
  If MessageNetWork.Id = 'New' Then Begin
    RandSeed := MessageNetWork.Init;
    GamerPole.Free;
    GamerPole := TField.Create(Point(GamerPoleLeft,GamerPoleTop), True);
    ComputerPole.Free;
    ComputerPole := TField.Create(Point(ComputerPoleLeft,ComputerPoleTop), False);
    CanPaint := True;
    GameGoes := True;
    HodGamer := Not MessageNetWork.WhoMove;
    If HodGamer Then
      Form1.StatusBar1.Panels[0].Text := msg1
    Else
      Form1.StatusBar1.Panels[0].Text := msg2;
    GamerPole.Show(Form1);
    ComputerPole.Show(Form1);
    For i:=1 To 10 Do Begin
      GamerPole.Ships[i].Show(false,GamerPole,Form1);
      ComputerPole.Ships[i].Show(True,ComputerPole,Form1);
    End;
  End;
  If MessageNetWork.Id = 'Shot' Then Begin
    If MessageNetWork.Crash Then Begin
      Shot.x := MessageNetWork.ShotX;
      Shot.y := MessageNetWork.ShotY;
      HodGamer := False;
      Form1.StatusBar1.Panels[0].Text := msg2;
      Game;
    End
    Else Begin
      Form1.StatusBar1.Panels[0].Text := msg1;
      ComputerPole.Fire(Point(MessageNetWork.ShotX,MessageNetWork.ShotY));
      GamerPole.Show(Form1);
      ComputerPole.Show(Form1);
      For i:=1 To 10 Do Begin
        GamerPole.Ships[i].Show(false,GamerPole,Form1);
        ComputerPole.Ships[i].Show(True,ComputerPole,Form1);
      End;
      HodGamer := True;
    End;
  End;
end;

//���� "� ���������"
procedure TForm1.N12Click(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

//���� "�������"
procedure TForm1.N11Click(Sender: TObject);
begin
  ShowMessage('������� ���� ��� �� ��������.');
end;

//���� "�����"
procedure TForm1.N7Click(Sender: TObject);
begin
  Close;
end;

//���� "���������"
procedure TForm1.N9Click(Sender: TObject);
begin
  If Form3.ShowModal = mrOk Then Begin
    If Form3.RadioButton1.Checked Then
      BrainComputer := swLow;
    If Form3.RadioButton2.Checked Then
      BrainComputer := swAverage;
    If Form3.RadioButton3.Checked Then
      BrainComputer := swHigh;
  End;
end;

//���� "����� ���������"
procedure TForm1.N2Click(Sender: TObject);
begin
  CanPaint := False;
  GameGoes := False;
  StatusGame := swOnce;
  Form1.OnCreate(Form1);
  NewGame;
end;

//���� "���� �� ����� ����������"
procedure TForm1.N3Click(Sender: TObject);
begin
  HasNewGame := True;
  GameGoes := False;
  CanPaint := False;
  Form2.Notebook1.PageIndex := 1;
  Form2.Button2.Caption := '������';
  Form2.Button1.Caption := 'Ok';
  Form2.Label1.Caption := '������� ��� ������� ������';
  Form2.Label2.Visible := True;
  Form2.Edit2.Visible := True;
  If Form2.ShowModal = mrOk Then Begin
    GamerName1 := Form2.Edit1.Text;
    GamerName2 := Form2.Edit2.Text;
    StatusGame := swHotSeat;
    Form1.OnCreate(Form1);
    NewGame;
  End;
end;

//���� "����� (������)"
procedure TForm1.N4Click(Sender: TObject);
begin
  CanPaint := False;
  GameGoes := False;
  StatusGame := swServer;
  Form1.OnCreate(Form1);
  NewGame;
end;

//���� "����� (������)"
procedure TForm1.N5Click(Sender: TObject);
begin
  CanPaint := False;
  GameGoes := False;
  Form2.Notebook1.PageIndex := 1;
  Form2.Button2.Caption := '������';
  Form2.Button1.Caption := 'Ok';
  Form2.Label1.Caption := '������� ��� �������';
  Form2.Label2.Visible := False;
  Form2.Edit2.Visible := False;
  If Form2.ShowModal = mrOk Then Begin
    HostName := Form2.Edit1.Text;
    StatusGame := swClient;
    Form1.OnCreate(Form1);
    NewGame;
  End;
end;

//���� "�������������� �����������"
procedure TForm1.N13Click(Sender: TObject);
begin
  If StatusGame = swOnce Then Begin
    N13.Checked := Not N13.Checked;
    If N13.Checked Then Begin
      BuildShip := swAutoBuild;
{      GroupBox1.Visible := False;}
    End
    Else Begin
      BuildShip := swNotAutoBuild;
{      GroupBox1.Visible := True;   }
    End
  End;
end;

end.
