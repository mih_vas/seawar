unit Running;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Main;

type
  TForm2 = class(TForm)
    Notebook1: TNotebook;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Notebook1PageChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.DFM}

procedure TForm2.Button1Click(Sender: TObject);
begin
  If (NoteBook1.PageIndex = 0) And ((RadioButton2.Checked) Or (RadioButton4.Checked)) Then  Begin
    NoteBook1.PageIndex := 1;
    Button2.Caption := '< �����';
  End
  Else Begin
    If RadioButton1.Checked Then
      StatusGame := swOnce;
    If RadioButton2.Checked Then  Begin
      GamerName1 := Edit1.Text;
      GamerName2 := Edit2.Text;
      StatusGame := swHotSeat;
    End;
    If RadioButton3.Checked Then
      StatusGame := swServer;
    If RadioButton4.Checked Then Begin
      HostName := Edit1.Text;
      StatusGame := swClient;
    End;
    Application.CreateForm(TForm1, Form1);
    Form1.Show;
    Form2.Hide;
  End;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  If NoteBook1.PageIndex = 0 Then  Begin
    Close;
  End
  Else Begin
    NoteBook1.PageIndex := 0;
    Button2.Caption := '������';
  End;
end;

procedure TForm2.Notebook1PageChanged(Sender: TObject);
begin
  If NoteBook1.PageIndex = 1 Then Begin
    If RadioButton2.Checked Then  Begin
      Label1.Caption := '������� ��� ������� ������';
      Label2.Visible := True;
      Edit2.Visible := True;
    End;
    If RadioButton4.Checked Then Begin
      Label1.Caption := '������� ��� �������';
      Label2.Visible := False;
      Edit2.Visible := False;
    End;
  End;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  BrainComputer := swHigh;
end;

end.
