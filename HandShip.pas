unit HandShip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Main, StdCtrls, ExtCtrls, Buttons, Menus, ImgList;

type
  TForm4 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Image4: TImage;
    Image3: TImage;
    Image2: TImage;
    Image1: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Label1: TLabel;
    v4: TImage;
    h3: TImage;
    v3: TImage;
    h2: TImage;
    v2: TImage;
    h4: TImage;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormPaint(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Image5Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image6Click(Sender: TObject);
    procedure Image7Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Image8Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure Image10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;
  HandPole :TField;  //����, �� ������� ����� ����������� �������
  ImageBox: Array [1..10] Of TImage;
  MoveShip: Boolean;
  CountShipsNotPlace: Byte;
  Stand: Array [1..10] Of Boolean;
  X_old, Y_old: Integer;

implementation

{$R *.dfm}

procedure TForm4.FormActivate(Sender: TObject);
begin
  CanPaint := False;
  HandShipOK := False;
  HandPole.Free;
  HandPole := TField.Create(Point(GamerPoleLeft,GamerPoleTop),True,BuildShip);
  HandPole.Show(Form4);
  MoveShip := False;
  CountShipsNotPlace := 10;
  Image1.Left := 270; Image1.Top := 15;
  Image1.Height := 20; Image1.Width := 80;
//  Image1.Picture.LoadFromFile('4h.bmp');
  Image1.Picture.Assign(h4.Picture);
  Image2.Left := 270; Image2.Top := 40;
  Image2.Height := 20; Image2.Width := 60;
//  Image2.Picture.LoadFromFile('3h.bmp');
  Image2.Picture.Assign(h3.Picture);
  Image3.Left := 270; Image3.Top := 65;
  Image3.Height := 20; Image3.Width := 40;
//  Image3.Picture.LoadFromFile('2h.bmp');
  Image3.Picture.Assign(h2.Picture);
  Image4.Left := 270; Image4.Top := 90;
  Image5.Left := 270; Image5.Top := 40;
  Image5.Height := 20; Image5.Width := 60;
//  Image5.Picture.LoadFromFile('3h.bmp');
  Image5.Picture.Assign(h3.Picture);
  Image6.Left := 270; Image6.Top := 65;
  Image6.Height := 20; Image6.Width := 40;
//  Image6.Picture.LoadFromFile('2h.bmp');
  Image6.Picture.Assign(h2.Picture);
  Image7.Left := 270; Image7.Top := 65;
  Image7.Height := 20; Image7.Width := 40;
//  Image7.Picture.LoadFromFile('2h.bmp');
  Image7.Picture.Assign(h2.Picture);
  Image8.Left := 270; Image8.Top := 90;
  Image9.Left := 270; Image9.Top := 90;
  Image10.Left := 270; Image10.Top := 90;
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CanPaint := True;
  MoveShip := False;
end;


procedure TForm4.FormPaint(Sender: TObject);
begin
  HandPole.Show(Form4);
end;

procedure TForm4.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  If MoveShip Then Begin
    Case WhatDeck Of
    41: Begin
         Image1.Left := X;
         Image1.Top := Y;
       End;
    31: Begin
         Image2.Left := X;
         Image2.Top := Y;
       End;
    32: Begin
         Image5.Left := X;
         Image5.Top := Y;
       End;
    21: Begin
         Image3.Left := X;
         Image3.Top := Y;
       End;
    22: Begin
         Image6.Left := X;
         Image6.Top := Y;
       End;
    23: Begin
         Image7.Left := X;
         Image7.Top := Y;
       End;
    11: Begin
         Image4.Left := X;
         Image4.Top := Y;
       End;
    12: Begin
         Image8.Left := X;
         Image8.Top := Y;
       End;
    13: Begin
         Image9.Left := X;
         Image9.Top := Y;
       End;
    14: Begin
         Image10.Left := X;
         Image10.Top := Y;
       End;
    End;
  End;
end;

Function Create_Map(k,xt,yt:Byte): Boolean; //���������� true - ���������
                                            //           false - �����������
Const
  MsgStr1: String = '��������� ������� �� ����, ����������' ;
  MsgStr2: String = '���� ������� ������������ � ������ �������� '+Chr(13)+'��� ��������� � ���������������� �������� �� ������� �������' ;
Var
  i, x, y, num: Byte;
  ori, peresecheniy_net: Boolean;
  place_ship: TPoint;
Begin
     //���������� �����
     Case k Of
     1     : num := 4;
     2,3   : num := 3;
     4..6  : num := 2;
     7..10 : num := 1;
     End;
     //���������� �������
     If num = 1 Then
       ori := True
     Else Begin
       If MessageDlg('������� ����� �������� ������������� (Yes) ��� �� ����������� (No)?',mtConfirmation, [mbYes, mbNo], 0) = mrYes Then
         ori := True
       Else
         ori := False;
     End;
     //���������� �������
     If ori Then Begin
       If ((xt>=HandPole.Place.X) And (yt>=HandPole.Place.Y)) And
          ((xt<=HandPole.Place.X+Cell_Width*10-num*Cell_Width) And (yt<=HandPole.Place.Y+Cell_Height*10-Cell_Height)) Then Begin  //������� �������� �� ����?
         x := (Round((xt - HandPole.Place.X) / Cell_Width) * Cell_Width) Div Cell_Width + 1;   //������������� ���������� �
         y := (Round((yt - HandPole.Place.Y) / Cell_Height) * Cell_Height) Div Cell_Height + 1;   //������������� ���������� Y
       End
       Else Begin   //���� ������� �� ����� �� ����
         ShowMessage(MsgStr1);     //������� �� ����
         Result := False;         //��������� �� ������ �� �����
         Exit;                    //����� �� �-��
       End;
     End
     Else Begin
       If ((xt>=HandPole.Place.X) And (yt>=HandPole.Place.Y)) And
          ((xt<=HandPole.Place.X+Cell_Width*10-Cell_Width) And (yt<=HandPole.Place.Y+Cell_Height*10-num*Cell_Height)) Then Begin  //������� �������� �� ����?
         x := (Round((xt - HandPole.Place.X) / Cell_Width) * Cell_Width) Div Cell_Width + 1;   //������������� ���������� �
         y := (Round((yt - HandPole.Place.Y) / Cell_Height) * Cell_Height) Div Cell_Height + 1;   //������������� ���������� Y
       End
       Else Begin   //���� ������� �� ����� �� ����
         ShowMessage(MsgStr1);     //������� �� ����
         Result := False;         //��������� �� ������ �� �����
         Exit;                    //����� �� �-��
       End;
     End;
     //�������� �� ����������� � ������� ���������
     peresecheniy_net := True;
     If ori Then Begin
       For i:=0 To num-1 Do
         If HandPole.Map[x+i,y] <> 0 Then peresecheniy_net := False;
     End
     Else Begin
       For i:=0 To num-1 Do
         If HandPole.Map[x,y+i] <> 0 Then peresecheniy_net := False;
     End;
     //���� ��� ����������� � ������� ���������
     If peresecheniy_net Then Begin
       //��������� MAP ���������� ������� �������
       If ori Then Begin
         For i:=0 To num-1 Do
           HandPole.Map[x+i,y] := k;
       End
       Else Begin
         For i:=0 To num-1 Do
           HandPole.Map[x,y+i] := k;
       End;
       //�������� ������� -1
       If ori Then Begin
         For i:=0 To num-1 Do Begin
           If i = 0 Then Begin
             HandPole.Map[x-1,y] := -1;
             HandPole.Map[x-1,y-1] := -1;
             HandPole.Map[x-1,y+1] := -1;
           End;
           HandPole.Map[x+i,y-1] := -1;
           HandPole.Map[x+i,y+1] := -1;
           If i = num-1 Then Begin
             HandPole.Map[x+num,y] := -1;
             HandPole.Map[x+num,y-1] := -1;
             HandPole.Map[x+num,y+1] := -1;
           End;
         End;
       End
       Else Begin
         For i:=0 To num-1 Do Begin
           If i = 0 Then Begin
             HandPole.Map[x,y-1] := -1;
             HandPole.Map[x-1,y-1] := -1;
             HandPole.Map[x+1,y-1] := -1;
           End;
           HandPole.Map[x-1,y+i] := -1;
           HandPole.Map[x+1,y+i] := -1;
           If i = num-1 Then Begin
             HandPole.Map[x,y+num] := -1;
             HandPole.Map[x-1,y+num] := -1;
             HandPole.Map[x+1,y+num] := -1;
           End;
         End
       End;
       //���������� �������
       place_ship.x := HandPole.place.x + Cell_Width * (x - 1);
       place_ship.y := HandPole.place.y + Cell_Height * (y - 1);
       //�������� �������
       HandPole.Ships[k] := TShip.Create(ori,num,place_ship);
       Result := True;
     End
     Else Begin   //���� �� ����������� � ������ ��������
       ShowMessage(MsgStr2);
       Result := False;
       Exit;
     End;
End;


procedure TForm4.Image1Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 41;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[1] := False;              //������� �� ����������
      X_old := Image1.Left;           //��������� �������������� ����������
      Y_old := Image1.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(1,Image1.Left,Image1.Top) Then Begin  //���� ��������� ������� �� �������
        Image1.Left := X_old;           //������������ �������������� ����������
        Image1.Top := Y_old;
        Stand[1] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image1.Left := Round((Image1.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image1.Top := Round((Image1.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[1] := True;               //������� ����������
        If HandPole.Ships[1].Orientaition Then Begin
          If Image1.Height > Image1.Width Then Begin
            temp := Image1.Height;
            Image1.Height := Image1.Width;
            Image1.Width := temp;
          End;
//          Image1.Picture.LoadFromFile('4h.bmp');
          Image1.Picture.Assign(h4.Picture);
        End
        Else Begin
          If Image1.Height < Image1.Width Then Begin
            temp := Image1.Height;
            Image1.Height := Image1.Width;
            Image1.Width := temp;
          End;
//          Image1.Picture.LoadFromFile('4v.bmp');
          Image1.Picture.Assign(v4.Picture);
        End;
      End;
    End;
end;

procedure TForm4.FormClick(Sender: TObject);
begin
    MoveShip := False;
end;

procedure TForm4.BitBtn1Click(Sender: TObject);
Var
  i: Byte;
begin
  HandShipOk := True;
  For i:=1 To 10 Do
    HandShipOk:= HandShipOk and Stand[i];
  If HandShipOk Then
    GamerPole := HandPole;
end;

procedure TForm4.Image2Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 31;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[2] := False;              //������� �� ����������
      X_old := Image2.Left;           //��������� �������������� ����������
      Y_old := Image2.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(2,Image2.Left,Image2.Top) Then Begin  //���� ��������� ������� �� �������
        Image2.Left := X_old;           //������������ �������������� ����������
        Image2.Top := Y_old;
        Stand[2] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image2.Left := Round((Image2.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image2.Top := Round((Image2.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[2] := True;               //������� ����������
        If HandPole.Ships[2].Orientaition Then Begin
          If Image2.Height > Image2.Width Then Begin
            temp := Image2.Height;
            Image2.Height := Image2.Width;
            Image2.Width := temp;
          End;
//          Image2.Picture.LoadFromFile('3h.bmp');
          Image2.Picture.Assign(h3.Picture);
        End
        Else Begin
          If Image2.Height < Image2.Width Then Begin
            temp := Image2.Height;
            Image2.Height := Image2.Width;
            Image2.Width := temp;
          End;
//          Image2.Picture.LoadFromFile('3v.bmp');
          Image2.Picture.Assign(v3.Picture);
        End;
      End;
    End;
end;

procedure TForm4.Image5Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 32;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[3] := False;              //������� �� ����������
      X_old := Image5.Left;           //��������� �������������� ����������
      Y_old := Image5.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(3,Image5.Left,Image5.Top) Then Begin  //���� ��������� ������� �� �������
        Image5.Left := X_old;           //������������ �������������� ����������
        Image5.Top := Y_old;
        Stand[3] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image5.Left := Round((Image5.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image5.Top := Round((Image5.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[3] := True;               //������� ����������
        If HandPole.Ships[3].Orientaition Then Begin
          If Image5.Height > Image5.Width Then Begin
            temp := Image5.Height;
            Image5.Height := Image5.Width;
            Image5.Width := temp;
          End;
//          Image5.Picture.LoadFromFile('3h.bmp');
          Image5.Picture.Assign(h3.Picture);
        End
        Else Begin
          If Image5.Height < Image5.Width Then Begin
            temp := Image5.Height;
            Image5.Height := Image5.Width;
            Image5.Width := temp;
          End;
//          Image5.Picture.LoadFromFile('3v.bmp');
          Image5.Picture.Assign(v3.Picture);
        End;
      End;
    End;
end;

procedure TForm4.Image3Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 21;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[4] := False;              //������� �� ����������
      X_old := Image3.Left;           //��������� �������������� ����������
      Y_old := Image3.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(4,Image3.Left,Image3.Top) Then Begin  //���� ��������� ������� �� �������
        Image3.Left := X_old;           //������������ �������������� ����������
        Image3.Top := Y_old;
        Stand[4] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image3.Left := Round((Image3.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image3.Top := Round((Image3.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[4] := True;               //������� ����������
        If HandPole.Ships[4].Orientaition Then Begin
          If Image3.Height > Image3.Width Then Begin
            temp := Image3.Height;
            Image3.Height := Image3.Width;
            Image3.Width := temp;
          End;
//          Image3.Picture.LoadFromFile('2h.bmp');
          Image3.Picture.Assign(h2.Picture);
        End
        Else Begin
          If Image3.Height < Image3.Width Then Begin
            temp := Image3.Height;
            Image3.Height := Image3.Width;
            Image3.Width := temp;
          End;
//          Image3.Picture.LoadFromFile('2v.bmp');
          Image3.Picture.Assign(v2.Picture);
        End;
      End;
    End;
end;

procedure TForm4.Image6Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 22;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[5] := False;              //������� �� ����������
      X_old := Image6.Left;           //��������� �������������� ����������
      Y_old := Image6.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(5,Image6.Left,Image6.Top) Then Begin  //���� ��������� ������� �� �������
        Image6.Left := X_old;           //������������ �������������� ����������
        Image6.Top := Y_old;
        Stand[5] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image6.Left := Round((Image6.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image6.Top := Round((Image6.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[5] := True;               //������� ����������
        If HandPole.Ships[5].Orientaition Then Begin
          If Image6.Height > Image6.Width Then Begin
            temp := Image6.Height;
            Image6.Height := Image6.Width;
            Image6.Width := temp;
          End;
//          Image6.Picture.LoadFromFile('2h.bmp');
          Image6.Picture.Assign(h2.Picture);
        End
        Else Begin
          If Image6.Height < Image6.Width Then Begin
            temp := Image6.Height;
            Image6.Height := Image6.Width;
            Image6.Width := temp;
          End;
//          Image6.Picture.LoadFromFile('2v.bmp');
          Image6.Picture.Assign(v2.Picture);
        End;
      End;
    End;
end;

procedure TForm4.Image7Click(Sender: TObject);
var
  temp: Integer;
begin
    WhatDeck := 23;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[6] := False;              //������� �� ����������
      X_old := Image7.Left;           //��������� �������������� ����������
      Y_old := Image7.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(6,Image7.Left,Image7.Top) Then Begin  //���� ��������� ������� �� �������
        Image7.Left := X_old;           //������������ �������������� ����������
        Image7.Top := Y_old;
        Stand[6] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image7.Left := Round((Image7.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image7.Top := Round((Image7.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[6] := True;               //������� ����������
        If HandPole.Ships[6].Orientaition Then Begin
          If Image7.Height > Image7.Width Then Begin
            temp := Image7.Height;
            Image7.Height := Image7.Width;
            Image7.Width := temp;
          End;
//          Image7.Picture.LoadFromFile('2h.bmp');
          Image7.Picture.Assign(h2.Picture);
        End
        Else Begin
          If Image7.Height < Image7.Width Then Begin
            temp := Image7.Height;
            Image7.Height := Image7.Width;
            Image7.Width := temp;
          End;
//          Image7.Picture.LoadFromFile('2v.bmp');
          Image7.Picture.Assign(v2.Picture);
        End;
      End;
    End;
end;

procedure TForm4.Image4Click(Sender: TObject);
begin
    WhatDeck := 11;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[7] := False;              //������� �� ����������
      X_old := Image4.Left;           //��������� �������������� ����������
      Y_old := Image4.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(7,Image4.Left,Image4.Top) Then Begin  //���� ��������� ������� �� �������
        Image4.Left := X_old;           //������������ �������������� ����������
        Image4.Top := Y_old;
        Stand[7] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image4.Left := Round((Image4.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image4.Top := Round((Image4.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[7] := True;               //������� ����������
      End;
    End;
end;

procedure TForm4.Image8Click(Sender: TObject);
begin
    WhatDeck := 12;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[8] := False;              //������� �� ����������
      X_old := Image8.Left;           //��������� �������������� ����������
      Y_old := Image8.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(8,Image8.Left,Image8.Top) Then Begin  //���� ��������� ������� �� �������
        Image8.Left := X_old;           //������������ �������������� ����������
        Image8.Top := Y_old;
        Stand[8] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image8.Left := Round((Image8.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image8.Top := Round((Image8.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[8] := True;               //������� ����������
      End;
    End;
end;

procedure TForm4.Image9Click(Sender: TObject);
begin
    WhatDeck := 13;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[9] := False;              //������� �� ����������
      X_old := Image9.Left;           //��������� �������������� ����������
      Y_old := Image9.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(9,Image9.Left,Image9.Top) Then Begin  //���� ��������� ������� �� �������
        Image9.Left := X_old;           //������������ �������������� ����������
        Image9.Top := Y_old;
        Stand[9] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image9.Left := Round((Image9.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image9.Top := Round((Image9.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[9] := True;               //������� ����������
      End;
    End;
end;

procedure TForm4.Image10Click(Sender: TObject);
begin
    WhatDeck := 14;
    MoveShip := Not MoveShip;
    If MoveShip Then Begin          //���� ������� �����������
      Stand[10] := False;              //������� �� ����������
      X_old := Image10.Left;           //��������� �������������� ����������
      Y_old := Image10.Top;
    End
    Else Begin                      //���� ������� ��������� �����������
      if not Create_Map(10,Image10.Left,Image10.Top) Then Begin  //���� ��������� ������� �� �������
        Image10.Left := X_old;           //������������ �������������� ����������
        Image10.Top := Y_old;
        Stand[10] := False;              //������� �� ����������
      End
      Else Begin        //���� ��������� �������
        Image10.Left := Round((Image10.Left - HandPole.Place.X) / Cell_Width) * Cell_Width + HandPole.Place.X;           //������������� ����� ����������
        Image10.Top := Round((Image10.Top - HandPole.Place.Y) / Cell_Height) * Cell_Height + HandPole.Place.Y;
        Stand[10] := True;               //������� ����������
      End;
    End;
end;

end.
